package com.faceless.projectmanagement.service;


import com.faceless.projectmanagement.model.User;

public interface UserService {
	public User findUserByEmail(String email);
	public void saveUser(User user);
}
