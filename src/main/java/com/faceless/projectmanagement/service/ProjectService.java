package com.faceless.projectmanagement.service;

import com.faceless.projectmanagement.model.Project;

import java.util.List;

public interface ProjectService {
    void saveProject(Project project);
    void updateProject(Project project);
    
    Project findByProjectId(int projectId);
    Project findByProjectName(String projectName);
    
    List<Project> findAll();
}
