package com.faceless.projectmanagement.model;

import java.sql.Date;

public class Task {
    private int projectID;
    private int taskID;

    private String taskName;
    public boolean isTaskDone;
    private Date taskDoneDate;

    public Task() {
    }

    public Task(int projectID, int taskID, String taskName, boolean isTaskDone, Date taskDoneDate) {
        this.projectID = projectID;
        this.taskID = taskID;
        this.taskName = taskName;
        this.isTaskDone = isTaskDone;
        this.taskDoneDate = taskDoneDate;
    }

    public Task(String taskName) {
        this.taskName = taskName;
    }

    public int getProjectID() {
        return projectID;
    }

    public void setProjectID(int projectID) {
        this.projectID = projectID;
    }

    public int getTaskID() {
        return taskID;
    }

    public void setTaskID(int taskID) {
        this.taskID = taskID;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public boolean getIsTaskDone() {
        return isTaskDone;
    }

    public void setIsTaskDone(boolean taskDone) {
        isTaskDone = taskDone;
    }

    public Date getTaskDoneDate() {
        return taskDoneDate;
    }

    public void setTaskDoneDate(Date taskDoneDate) {
        this.taskDoneDate = taskDoneDate;
    }

    @Override
    public String toString() {
        return "Task{" +
                "projectID=" + projectID +
                ", taskID=" + taskID +
                ", taskName='" + taskName + '\'' +
                ", isTaskDone=" + isTaskDone +
                ", taskDoneDate=" + taskDoneDate +
                '}';
    }
}
