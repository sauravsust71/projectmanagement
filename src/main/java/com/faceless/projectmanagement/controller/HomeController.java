package com.faceless.projectmanagement.controller;


import com.faceless.projectmanagement.model.Project;
import com.faceless.projectmanagement.model.Task;
import com.faceless.projectmanagement.model.User;
import com.faceless.projectmanagement.service.ProjectService;
import com.faceless.projectmanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Controller
public class HomeController {
    @Autowired
    ProjectService projectService;
    @Autowired
    UserService userService;

//    @Autowired
//    EmployeeService employeeService;
//
//    @RequestMapping("/createEmployee")
//    public String showCreateEmployee(Model model){
//        model.addAttribute("employee",new Employee());
//        return "createEmployee";
//    }
//
//    @RequestMapping(value = "/employeeList" ,method = RequestMethod.GET)
//    public String showEmployeeList(Model model){
//        PageRequest limit = new PageRequest(0,10);
//
//        model.addAttribute("employees",employeeService.findAll(limit).getContent());
//        model.addAttribute("successMessage", "User has been registered successfully");
//
//        return "employeeList";
//
//    }
//
//    @RequestMapping("/saveEmployee")
//    public String showSaveEmployee(@RequestParam("photo") MultipartFile photo, @Valid Employee employee, BindingResult bindingResult) throws IOException {
//        ModelAndView modelAndView = new ModelAndView();
//        if(bindingResult.hasErrors()){
//            System.out.println(bindingResult.toString());
//        }
//        employee.setPhoto(photo.getBytes());
//
//        modelAndView.addObject("successMessage", "User has been registered successfully");
//        employeeService.saveEmployee(employee);
//
//
//        System.out.println(employee);
//        return "redirect:employeeList";
//    }
//
//    @RequestMapping("/updateEmployee")
//    public String showUpdateEmployee(@RequestParam("photo") MultipartFile photo, @Valid Employee employee, BindingResult bindingResult) throws IOException {
//
//        if(!photo.isEmpty())
//            employee.setPhoto(photo.getBytes());
//        if(bindingResult.hasErrors()){
//            System.out.println(bindingResult.toString());
//
//        }
//        employeeService.updateUser(employee);
//        ModelAndView modelAndView = new ModelAndView();
//
//
//        modelAndView.addObject("successMessage", "Employee has been updated successfully");
//
//        return "redirect:employeeList";
//    }
//
//
//
//    @RequestMapping("/edit/{id}")
//    public String showEdit(@PathVariable("id")int id , Model model){
//        //Employee employee = employeeService.findByName("eaimanshoshi");
//        Employee employee = employeeService.findById(id);
//        model.addAttribute("employee",employee);
//
//
//
//
//
//        return "employeeInfo";
//    }
//
//    @RequestMapping(value = "/image/{id}", produces = MediaType.IMAGE_PNG_VALUE)
//    public ResponseEntity<byte[]> getImage(@PathVariable("id") int id) throws IOException {
//
//        Employee employee = employeeService.findById(id);
//        final HttpHeaders headers = new HttpHeaders();
//        headers.setContentType(MediaType.IMAGE_PNG);
//
//
//        System.out.println("image returned");
//        return new ResponseEntity<byte[]>(employee.getPhoto(), headers, HttpStatus.OK);
//    }

    @RequestMapping("/projectList")
    public String showProjectList(Model model){
        model.addAttribute("projects",projectService.findAll());
        return "projectList";
    }

    @RequestMapping("/createProject")
    public String showCreateProject(Model model){
        model.addAttribute("project",new Project());
        return "createProject";
    }

    @RequestMapping(value = "/saveProject",method = RequestMethod.POST)
    public String saveEmployee(@RequestParam("photo") MultipartFile photo, Project project, BindingResult bindingResult,Model model) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        project.setLastUpdateBy(user.getName());
        project.setPhoto(photo.getBytes());


        //generating Task class on taskName
        List<Task> taskList = new ArrayList<>();
        Iterator<String> iterator = project.getTaskNames().iterator();
        while (iterator.hasNext()){
            taskList.add(new Task(iterator.next()));
        }
        project.setTaskList(taskList);
        System.out.println(project);

        //projectService.saveProject(project);

        model.addAttribute("project",project);

        return "editProject";

    }

    @RequestMapping(value = "/updateProject")
    public String updateEmployee(Project project,Model model){
        System.out.println("From update "+project.toString());
        model.addAttribute("project",project);
        return "editProject";

    }


}
